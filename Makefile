EMACS=emacs
EMACS23=emacs23
EMACS-OPTIONS=

ELS  = flx.el
ELS += flx-ido.el
TEST_ELCS = $(wildcard tests/*.elc)

ELCS = $(ELS:.el=.elc)

.PHONY: test test-nw travis-ci show-version before-test clean

all: $(ELCS)

clean:
	$(RM) $(ELCS) $(TEST_ELCS)

show-version:
	echo "*** Emacs version ***"
	echo "EMACS = `which ${EMACS}`"
	${EMACS} --version

before-test: show-version

test: before-test
	${EMACS} -batch -Q -L /usr/share/emacs/site-lisp/elpa-src/async-* \
		-l tests/run-test.el

test-nw: before-test
	${EMACS} -batch -Q -nw -L /usr/share/emacs/site-lisp/elpa-src/async-* \
		-l tests/run-test.el

travis-ci: before-test
	echo ${EMACS-OPTIONS}
	${EMACS} -batch -Q -l tests/run-test.el

%.elc: %.el
	${EMACS} -batch -Q -L . -f batch-byte-compile $<
